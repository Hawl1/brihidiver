# **Brick hill discord verifier bot by Hawli**

**This project has been archived due to latest Brick Hill shutdown, you can find [more information](https://blog.brick-hill.com/reducing-services/) right here.**

## Features:
- Can verify people
- Licensed under GPLv3
- Self-hostable (like you do in your Brick Hill sets)

## HOW THE HECK IM GONNA HOST THIS
Theres are services i can recommend for it
1. [Vercel](https://vercel.com/)
2. [Render](https://render.com/)
3. [SolarHosting](https://solarhosting.cc/) for hosting the bot.

**Note:** Vercel and Render can be tricky since they require you a Gitlab or Github account.

## How to setup:
1. Clone the repository

```sh
git clone https://gitlab.com/Hawl1/brihidiver.git
```

2. You will need to edit the .env file for it

Use your favorite text editor for that (Notepad, Vim, Emacs, VSCode etc..)

### Things inside the .env file

**TOKEN**
This is the TOKEN of the bot you will host, make sure you invited the bot to the server you want to setup

**GUILD_ID**
This is the ID of the server you will setup the bot into.

**VERIFIED_ROLE_ID**
This is ID of the role when user gets verified

3. Running the bot

Run this script

```sh
pip3 install -r requirements.txt && python3 main.py
```
