import sqlite3
from dotenv import dotenv_values
import discord
from discord import app_commands
import random
import string
import httpx
import os
import time

uptime = round(int(time.time()))

async def get_id(name):
    async with httpx.AsyncClient() as client:
        response = await client.get(f"https://api.brick-hill.com/v1/user/id?username={name}")
        json = response.json()
        return json["id"]
        await client.aclose()


async def get_desc(id):
    async with httpx.AsyncClient() as client:
        response = await client.get(f"https://api.brick-hill.com/v1/user/profile?id={id}")
        return response.json()["description"]
        await client.aclose()

if os.path.exists("user.db"):
    os.remove("user.db")

userdb = sqlite3.connect("user.db")

userdb.execute(f"""CREATE TABLE Users (
    DISCORD_ID INTEGER,
    BRICK_HILL_NAME TEXT,
    VERIFY_CODE TEXT
)""")

userdb.commit()
userdb.close()

env_vars = dotenv_values(".env")
guild_id = env_vars["GUILD_ID"]

intents = discord.Intents.default()
client = discord.Client(intents=intents)
tree = app_commands.CommandTree(client)

@tree.command(name = "uptime", description = "Gives info about uptime", guild=discord.Object(id=guild_id))
async def verify_command(interaction):
    await interaction.response.send_message(f"Servers didn't fucked up for <t:{uptime}:R>.")
    

@tree.command(name = "verify", description = "Get verified", guild=discord.Object(id=guild_id))
async def verify_command(interaction, name: str):
    dm = await interaction.user.create_dm()
    userdb = sqlite3.connect("user.db")
    
    characters = string.ascii_letters + string.digits
    random_string = ''.join(random.choice(characters) for _ in range(15))
    random_string = "verify-by-hawli-" + random_string

    userdb.execute(f"""INSERT INTO Users (DISCORD_ID, BRICK_HILL_NAME, VERIFY_CODE)
    VALUES (?, ?, ?)
    """, (int(interaction.user.id), str(name), str(random_string)))
    
    userdb.commit()
    userdb.close()

    embed = discord.Embed(
    title="Hey!",
    description="Check DMs now!",
    color=discord.Color.light_gray()
    )

    await interaction.response.send_message(embed=embed, ephemeral=True)
    await dm.send(f"Your brick hill verify code is `{random_string}` paste this on your brick hill description then run /confirm .")

@tree.command(name = "confirm", description = "Confirm verify", guild=discord.Object(id=guild_id))
async def verify_command(interaction):
    userdb = sqlite3.connect("user.db")
    cursor = userdb.cursor()

    cursor.execute("SELECT * FROM Users WHERE DISCORD_ID=?", (interaction.user.id,))
    row = cursor.fetchone()
    
    if row == None:
        embed = discord.Embed(
        title="You dumbass",
        description="You will run /verify first",
        color=discord.Color.red()
        )
        await interaction.response.send_message(embed=embed, ephemeral=True)
    else:
        for data in row:
            global id, bh_name, verify_code
            id = row[0]
            bh_name = row[1]
            verify_code = row[2]

        cursor.execute("DELETE FROM Users WHERE DISCORD_ID=?", (interaction.user.id,))
        userdb.commit()
        userdb.close()
        
        bh_id = await get_id(bh_name)
        bh_desc = await get_desc(int(bh_id))
        
        if verify_code in bh_desc:
            embed = discord.Embed(
            title="Verified successfully!",
            description="You will get @Verified role in a second, bot's developer wishes you a good time on the server.",
            color=discord.Color.green()
            )
            await interaction.response.send_message(embed=embed, ephemeral=True)
            role = discord.utils.get(interaction.guild.roles, id=int(env_vars["VERIFIED_ROLE_ID"]))
            await interaction.user.add_roles(role, reason="Got verified.")
            await interaction.user.edit(nick=bh_name)
        else:
            embed = discord.Embed(
            title="Can't verify",
            description="Because you dumbass didnt put the verify code on your Brick Hill account description. Now i'm punishing you to call /bhverify again.",
            color=discord.Color.red()
            )
            await interaction.response.send_message(embed=embed, ephemeral=True)


@client.event
async def on_ready():
    await tree.sync(guild=discord.Object(id=guild_id))

client.run(env_vars["TOKEN"])
